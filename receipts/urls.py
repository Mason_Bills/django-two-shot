from receipts.views import receipt_list, create_receipt
from accounts.views import user_login
from django.urls import path

urlpatterns = [
    path("", receipt_list, name="home"),
    path("login/", user_login, name="login"),
    path("create/", create_receipt, name="create_receipt"),
]
